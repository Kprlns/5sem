//Капралов Н.С. 8О-308-Б
//Лабораторная работа 7
//Кривая Безье 5го порядка
//Главный класс
package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Капралов Н.С. Курсовая работа. Линейная поврхность Кунса, ограниченная кардинальными сплайнами");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
