//Created by Kapralov Nikita
//Computer Graphics Lab 1
//Контроллер, обеспечивающий поддержку графического интерфейса

package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

public class Controller {

    @FXML
    private AnchorPane canvasHolder;

    @FXML
    private Slider aSlider;

    @FXML
    private Slider bSlider;

    @FXML
    private Slider stepSlider;

    @FXML
    private Slider rotationSlider;

    @FXML
    void initialize() {
        ResizableCanvas canvas = new ResizableCanvas(canvasHolder.getPrefWidth(),canvasHolder.getPrefHeight());
        //холст для отрисовки графика
        canvas.setBackgroundColor(Color.WHITE);
        canvas.setPencilColor(Color.BLACK);
        canvas.setTickColor(Color.rgb(232,232,232));

        //привязка к границам окна
        AnchorPane.setTopAnchor(canvas, 0.);
        AnchorPane.setBottomAnchor(canvas, 0.);
        AnchorPane.setLeftAnchor(canvas, 0.);
        AnchorPane.setRightAnchor(canvas, 0.);

        canvasHolder.getChildren().add(canvas);
        canvas.setStep(0.05);
        canvas.setA(0);
        canvas.setB(0);

        //отслеживание изменений значений на слайдерах
        aSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setA(aSlider.getValue());
            canvas.resize(canvas.getWidth(),canvas.getWidth());
        });

        bSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setB(bSlider.getValue());
            canvas.resize(canvas.getWidth(),canvas.getWidth());
        });

        stepSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
                canvas.setStep(Math.max(stepSlider.getValue(), 0.05));
                canvas.resize(canvas.getWidth(), canvas.getHeight());
        });

        rotationSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setRotation(rotationSlider.getValue() * Math.PI / 180);
            canvas.resize(canvas.getWidth(),canvas.getHeight());
        });
    }

}


