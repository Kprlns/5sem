//Created by Kapralov Nikita
//Computer Graphics Lab 1
//Матрица для линейных преобразований

package sample;

public class TransformMatrix {
    private double matrix[][];

    TransformMatrix(double x1, double y1, double x2, double y2) {
        matrix = new double[2][2];
        matrix[0][0] = x1;
        matrix[0][1] = y1;
        matrix[1][0] = x2;
        matrix[1][1] = y2;
    }

    public Point TransformPoint(Point point) {
        return new Point(matrix[0][0] * point.getX() + matrix[0][1] * point.getY(),
                matrix[1][0] * point.getX() + matrix[1][1] * point.getY());
    }

}
