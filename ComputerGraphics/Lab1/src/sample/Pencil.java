//Created by Kapralov Nikita
//Computer Graphics Lab 1
//Класс, занимающийся рисовкой на холста

package sample;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Vector;


public class Pencil {
    private final ResizableCanvas canvas;

    private Point zero; //центр координат
    private int scale;
    private double rotation;
    private double step; //шаг аппроксимации
    private double b;
    private double a;
    private Vector<Point> points; //точки графика

    private Color color, tickColor, chartColor;

    Pencil(ResizableCanvas canvas) {
        this.canvas = canvas;
        zero = new Point((int)canvas.getWidth() / 2,(int)canvas.getHeight() / 2);
        scale = (int)(Math.min(canvas.getWidth(),canvas.getHeight()) / 14);
    }
    public void setTickColor(Color color) {
        tickColor = color;
    }
    public void setPencilColor(Color color) {
        this.color = color;
    }
    public void setChartColor(Color graphicColor) {
        this.chartColor = graphicColor;
    }

    public void drawLine(int x1, int y1, int x2, int y2){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(color);
        gc.strokeLine(x1,y1,x2,y2);
    }

    public void drawLine(int x1, int y1, int x2, int y2, Color color){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setStroke(color);
        gc.strokeLine(x1,y1,x2,y2);
    }

    public void drawTick(int xLength, int yLength) {//рисовка риски на осях и сетку
        GraphicsContext gc = canvas.getGraphicsContext2D();
        //gc.setStroke(tickColor);

        for(int i = 1; i <= xLength; i++) {//прорисовка рисок на оси абсцисс и вертикальных составляющих сетки
            gc.setStroke(tickColor);
            gc.strokeLine(zero.getX() + i * scale, 0, zero.getX() + i * scale, 2 * zero.getY());
            gc.strokeLine(zero.getX() - i * scale, 0, zero.getX() - i * scale, 2 * zero.getY());
            gc.setStroke(color);
            drawLine((int)(zero.getX() + i * scale), (int)zero.getY() - 4, (int)(zero.getX() + i * scale), (int)(zero.getY() + 4));
            drawLine((int)(zero.getX() - i * scale), (int)zero.getY() - 4, (int)(zero.getX() - i * scale), (int)(zero.getY() + 4));
        }

        for (int i = 1; i <= yLength; i++) {//прорисовка рисок на оси ординат и горизотельных составляющих сетки
            gc.setStroke(tickColor);
            gc.strokeLine(0, (int)(zero.getY() + i * scale), (int)(2 * zero.getX()), (int)(zero.getY() + i * scale));
            gc.strokeLine(0, (int)(zero.getY() - i * scale), (int)(2 * zero.getX()), (int)(zero.getY() - i * scale));
            gc.setStroke(color);
            drawLine((int)(zero.getX() - 4), (int)(zero.getY() + i * scale), (int)(zero.getX() + 4), (int)(zero.getY() + i * scale));
            drawLine((int)(zero.getX() - 4), (int)(zero.getY() - i * scale), (int)(zero.getX() + 4), (int)(zero.getY() - i * scale));

        }
    }

    public void drawAxis() {
        int xLength = ((int)canvas.getWidth() / 2) / scale;
        int yLength = ((int)canvas.getHeight() / 2) / scale;
        drawLine(0 , (int)zero.getY(), (int)(2 * zero.getX()), (int)zero.getY());
        drawLine((int)zero.getX(), 0, (int)zero.getX(), (int)(2 * zero.getY()));
        drawTick(xLength,yLength);
    }

    public void countPoints() { //рассчитать значения функции с заданным шагом
        points = new Vector<>(10,10);
        int i = 0;
        for (double arg = 0; arg < 2 * Math.PI; arg += step, i++) {
            points.add(i, new Point(a * Math.cos(arg), b * Math.sin(arg)));
        }
    }

    public void drawChart() {
        TransformMatrix matrix = new TransformMatrix(scale, 0, 0, scale);
        TransformMatrix rotationMatrix = new TransformMatrix(Math.cos(rotation), Math.sin(rotation),
                -1 * Math.sin(rotation), Math.cos(rotation));
        Point p1, p2;
        Color color = Color.BLUE;
        p1 = matrix.TransformPoint(points.get(0));
        p1 = rotationMatrix.TransformPoint(p1);


        for (int i = 1; i < points.size(); i++) {
            p2 = matrix.TransformPoint(points.get(i));
            p2 = rotationMatrix.TransformPoint(p2);

            drawLine((int) (zero.getX() + p1.getX()), (int) (zero.getY() + p1.getY()),
                    (int) (zero.getX() + p2.getX()), (int) (zero.getY() + p2.getY()), color);
            p1 = p2;
        }
        p1 = matrix.TransformPoint(points.get(0));
        p1 = rotationMatrix.TransformPoint(p1);

        p2 = matrix.TransformPoint(points.get(points.size() - 1));
        p2 = rotationMatrix.TransformPoint(p2);

        drawLine((int) (zero.getX() + p1.getX()), (int) (zero.getY() + p1.getY()),
                (int) (zero.getX() + p2.getX()), (int) (zero.getY() + (int) p2.getY()), color);
    }


    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }
    public double getStep() {
        return step;
    }

    public void setStep(double step) {
        this.step = step;
    }

    public double getRotation() {
        return rotation;
    }
    public void setRotation(double rotation) {
        this.rotation = rotation;
    }
}
