//Created by Kapralov Nikita
//Computer Graphics Lab 1
package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ResizableCanvas extends Canvas {
    @Override
    public boolean isResizable() {
        return true;
    }

    private Color backgroundColor;
    private Color pencilColor;
    private Color tickColor;
    private Color chartColor;

    private double a;
    private double b;
    private double step;
    private double rotation;

    public ResizableCanvas(double width, double height) {
        super(width, height);
    }

    public void setBackgroundColor(Color color) {
        this.backgroundColor = color;
    }
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setPencilColor(Color color) {
        this.pencilColor = color;
    }
    public Color getPencilColor() {
        return pencilColor;
    }

    public void setTickColor(Color color) {
        this.tickColor = color;
    }
    public Color getTickColor() {
        return tickColor;
    }

    public void resize(double width,double height){
        setWidth(width);
        setHeight(height);
        GraphicsContext gc = this.getGraphicsContext2D();
        gc.setFill(backgroundColor);
        gc.fillRect(0,0,this.getWidth(),this.getHeight());

        Pencil pencil = new Pencil(this);
        pencil.setPencilColor(pencilColor);
        pencil.setTickColor(tickColor);
        pencil.setA(a);
        pencil.setB(b);
        pencil.setStep(step);
        pencil.setRotation(rotation);
        pencil.drawAxis();
        pencil.countPoints();
        pencil.drawChart();
    }
    public double getA() {
        return a;
    }
    public void setA(double a) {
        this.a = a;
    }


    public double getB() {
        return b;
    }
    public void setB(double b) {
        this.b = b;
    }

    public double getStep() {
        return step;
    }
    public void setStep(double step) {
        this.step = step;
    }

    public double getRotation() {
        return rotation;
    }
    public void setRotation(double rotate) {
        this.rotation = rotate;
    }

    public Color getChartColor() {
        return chartColor;
    }
    public void setChartColor(Color chartColor) {
        this.chartColor = chartColor;
    }
}
