//Created by Kapralov Nikita
//Computer Graphics Lab 1
//Main program file

package sample;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample1.fxml"));
        //подключение файла с разеткой
        Scene scene = new Scene(root);
        primaryStage.setTitle("Kapralov N.S. Chart: x = a·cos(t), y = b·sin(t)");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setMinWidth(scene.getWindow().getWidth());
        primaryStage.setMinHeight(scene.getWindow().getHeight());

    }


    public static void main(String[] args) {
        launch(args);
    }
}
