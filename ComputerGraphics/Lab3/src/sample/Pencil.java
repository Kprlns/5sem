//Капралов Н.С. 8о-308Б
//Класс, выполняющий рисовку на холсте

package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import java.util.Vector;


public class Pencil {
    private final ResizableCanvas canvas;

    public Point zero;
    public int scale;

    double step;

    public Point[] axis;

    public Point[] bottom;
    public Point[] top;

    TransformMatrix matrix;

    Pencil(ResizableCanvas canvas) {
        this.canvas = canvas;

        scale = (int)(Math.min(canvas.getWidth(),canvas.getHeight()) / 3);
        zero = new Point((int)canvas.getWidth() / 2,(int)canvas.getHeight() / 2,0,1);

        matrix = new TransformMatrix(scale);

        axis = new Point[3];
        axis[0] = new Point(1,0,0,1);
        axis[1] = new Point(0,1,0,1);
        axis[2] = new Point(0,0,1,1);

        bottom = new Point[] {
                new Point(1,1,0,1),
                new Point(1,-1,0,1),
                new Point(-1,-1,0,1),
                new Point(-1,1,0,1)
        };

        top = new Point[] {
                new Point(1,1,4,1),
                new Point(1,-1,4,1),
                new Point(-1,-1,4,1),
                new Point(-1,1,4,1)
        };
    }

    public void drawLine(int x1, int y1, int x2, int y2, Color color){
        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setStroke(color);
        gc.strokeLine(x1,y1,x2,y2);
    }
    public void drawLine(int x2, int y2, Color color) {
        drawLine((int)zero.getX(), (int)zero.getY(), x2, y2, color);
    }

    public void drawLine(Point a, Point b, Color color) {
        drawLine((int)(zero.getX() + scale * a.getX()), (int)(zero.getY() - scale * a.getY()),
                 (int)(zero.getX() + scale * b.getX()), (int)(zero.getY() - scale * b.getY()), color);
    }


    public void drawAxis(TransformMatrix matrix, Color[] colors, boolean flag) {
        //отрисовка осей
        zero.setX(canvas.getWidth() / 2);
        zero.setY(canvas.getHeight() / 2);

        if(flag) {
            axis = new Point[]{new Point(1,0,0,1), new Point(0,1,0,1), new Point(0,0,1,1)};
        }
        for (int i = 0; i < 3; i++) {
            axis[i] = matrix.TransformPoint(axis[i]);
            drawLine((int)(zero.getX() + scale * axis[i].getX()), (int)(zero.getY() - scale * axis[i].getY()), colors[i]);
        }
    }

    Vector<Point> countCirle(double z, double step, TransformMatrix matrix) {
        //рассчет точек заданной окружности
        Vector<Point> res = new Vector<>();
        double r = Math.sqrt(1 - z * z);
        double step1 = 0;
        while (step1 < Math.PI * 2) {
            res.add(matrix.TransformPoint(new Point(r * Math.cos(step1), r * Math.sin(step1), z, 1)));
            step1 += step;
        }
        step1 = 0;
        res.add(matrix.TransformPoint(new Point(r * Math.cos(step1), r * Math.sin(step1), z, 1)));
        return res;
    }

    public void drawFigure(TransformMatrix matrix, Color color, Color[] colors, double step) { //отрисовка фигуры
        zero = new Point((int)canvas.getWidth() / 2,(int)canvas.getHeight() / 2,0,1);
        Vector<Vector<Point>> edges = new Vector<>();

        //рассчет необходимых точек в соотвествии с текущими параметрами
        double z = -1;
        for (int i = 0; i < 3; i++) {
            edges.add(countCirle(z, step, matrix));
            z += (step / 3);
        }
        if(z + step > 0) {
            edges.add(countCirle(0, step, matrix));
            z = step;
        }
        while(z < 1 - step) {
            edges.add(countCirle(z, step, matrix));
            z += step;
        }
        z = 1 - step;
        for (int i = 0; i < 3; i++) {
            z += (step / 3);
            edges.add(countCirle(z, step, matrix));

        }

        for (int i = 0; i < edges.size() - 1; i++) {
            Vector<Point> circle1 = edges.get(i);
            Vector<Point> circle2 = edges.get(i + 1);
            for (int j = 0; j < circle1.size() - 1; j++) {
                //drawLine(circle1.get(j),circle1.get(j + 1),Color.BLACK);
                if(i != 0) {
                    drawTriangle(circle1.get(j), circle1.get(j + 1), circle2.get(j), color, matrix, i);
                }
                drawTriangle(circle2.get(j),circle2.get(j + 1), circle1.get(j + 1), color, matrix, i);
            }
        }
    }

    public void drawTriangle(Point a, Point b, Point c, Color color, TransformMatrix matrix, int i) {
        //отрисовка треугольника
        double vis = triangleVisible(a,b,c,matrix);
        if(vis != 0) {
            color = Color.color(Math.min((color.getRed() * vis), 1.),
                              Math.min((color.getGreen() * vis), 1.),
                              Math.min((color.getBlue() * vis), 1.));

            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setStroke(color);

            gc.strokeLine(zero.getX() + scale * a.getX(), zero.getY() - scale * a.getY(),
                    zero.getX() + scale * b.getX(), zero.getY() - scale * b.getY());
            gc.strokeLine(zero.getX() + scale * a.getX(), zero.getY() - scale * a.getY(),
                    zero.getX() + scale * c.getX(), zero.getY() - scale * c.getY());
            gc.strokeLine(zero.getX() + scale * b.getX(), zero.getY() - scale * b.getY(),
                    zero.getX() + scale * c.getX(), zero.getY() - scale * c.getY());

            gc.setFill(color);
            gc.fillPolygon(new double[] {zero.getX() + scale * a.getX(), zero.getX() + scale * b.getX(), zero.getX() + scale * c.getX()},
                           new double[] {zero.getY() - scale * a.getY(), zero.getY() - scale * b.getY(), zero.getY() - scale * c.getY()},
                     3);
        }
    }

    public double triangleVisible(Point a, Point b, Point c, TransformMatrix matrix) {//проверка треугольника на видимость

        //два направляющих вектора плоскости
        Point vector1 = new Point(b.getX() - a.getX(), b.getY() - a.getY(), b.getZ() - a.getZ(), 1);
        Point vector2 = new Point(c.getX() - a.getX(), c.getY() - a.getY(), c.getZ() - a.getZ(), 1);

        Point vector3 = new Point(0.2, 0.2, 0.2, 1);
        Point visor = new Point(0, 0, 1.2, 1);
        vector3 = matrix.TransformPoint(vector3);
        //внутренний вектор
        vector3 = new Point(a.getX() - vector3.getX(), a.getY() - vector3.getY(), a.getZ() - vector3.getZ(), 1);

        //нормаль
        Point normal = crossProduct(vector1, vector2);
        int k = 1;
        if (scalarProduct(vector3, normal) < 0) {
            k = -1;
        }

        double sp = scalarProduct(normal, visor);
        if (sp * k < 0) {
            return Math.abs(sp / (visor.length() * normal.length()));
        }

        return 0;
        //return true;
    }

    private double scalarProduct(Point a, Point b) { //скалярное произведение
        double x = a.getX() * b.getX();
        double y = a.getY() * b.getY();
        double z = a.getZ() * b.getZ();
        return x + y + z;
    }

    private Point crossProduct(Point a, Point b) { //векторное произведение
        double x = a.getY() * b.getZ() - a.getZ() * b.getY();
        double y = a.getZ() * b.getX() - a.getX() * b.getZ();
        double z = a.getX() * b.getY() - a.getY() * b.getX();
        return new Point(x, y, z,1);
    }

}
