//Капралов Н.С. 8о-308Б
//Класс-контроллер

package sample;


import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

public class Controller {

    @FXML
    private AnchorPane canvasHolder;

    @FXML
    private Slider stepSlider;

    @FXML
    private Slider redSlider;

    @FXML
    private Slider greenSlider;

    @FXML
    private Slider blueSlider;

    @FXML
    void initialize() {
        ResizableCanvas canvas = new ResizableCanvas(canvasHolder.getPrefWidth(),canvasHolder.getPrefHeight());
        canvas.backgroundColor = Color.WHITE;

        AnchorPane.setTopAnchor(canvas, 0.);
        AnchorPane.setBottomAnchor(canvas, 0.);
        AnchorPane.setLeftAnchor(canvas, 0.);
        AnchorPane.setRightAnchor(canvas, 0.);

        canvasHolder.getChildren().add(canvas);

        stepSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            double step = Math.max(0.1, stepSlider.getValue());
            canvas.setStep(step);
            canvas.resize(canvas.getWidth(),canvas.getHeight());
        });

        redSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setRed((int)redSlider.getValue());
            canvas.resize(canvas.getWidth(), canvas.getHeight());
        });
        greenSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setGreen((int)greenSlider.getValue());
            canvas.resize(canvas.getWidth(), canvas.getHeight());
        });
        blueSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvas.setBlue((int)blueSlider.getValue());
            canvas.resize(canvas.getWidth(), canvas.getHeight());
        });

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            canvas.xStart = event.getScreenX();
            canvas.yStart = event.getScreenY();
            canvas.xEnd = event.getScreenX();
            canvas.yEnd = event.getScreenY();
        });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            canvas.xEnd = event.getScreenX();
            canvas.yEnd = event.getScreenY();
            canvas.resize(canvas.getWidth(),canvas.getHeight());
            canvas.xStart = canvas.xEnd;
            canvas.yStart = canvas.yEnd;
        });

    }


}


