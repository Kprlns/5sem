/*
Капралов Н.С. 8о-308Б
Отрисовка фигуры с наложением освещения и скрытием невидимых линий
 */

package sample;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //загрузка fxml файла и начало работы
        Parent root = FXMLLoader.load(getClass().getResource("sample1.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setTitle("Капралов Н.С. Шар");
        primaryStage.setScene(scene);

        primaryStage.show();
        primaryStage.setMinWidth(scene.getWindow().getWidth());
        primaryStage.setMinHeight(scene.getWindow().getHeight());

    }


    public static void main(String[] args) {
        launch(args);
    }
}
