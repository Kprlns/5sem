//Капралов Н. 8о-308Б

package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

public class Controller {

    @FXML
    private AnchorPane canvasHolder;

    @FXML
    private Slider xRotation;

    @FXML
    private Slider yRotation;

    @FXML
    void initialize() {
        ResizableCanvas canvas = new ResizableCanvas(canvasHolder.getPrefWidth(),canvasHolder.getPrefHeight());
        canvas.backgroundColor = Color.WHITE;

        AnchorPane.setTopAnchor(canvas, 0.);
        AnchorPane.setBottomAnchor(canvas, 0.);
        AnchorPane.setLeftAnchor(canvas, 0.);
        AnchorPane.setRightAnchor(canvas, 0.);

        canvasHolder.getChildren().add(canvas);

        xRotation.valueProperty().addListener((observable, oldValue, newValue) -> xRotation.setValue((int)(canvas.xAngle * 180 / Math.PI) % 360));

        yRotation.valueProperty().addListener((observable, oldValue, newValue) -> yRotation.setValue((int)(canvas.yAngle * 180 / Math.PI) % 360));


        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            canvas.xStart = event.getScreenX();
            canvas.yStart = event.getScreenY();
            canvas.xEnd = event.getScreenX();
            canvas.yEnd = event.getScreenY();
        });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            canvas.xEnd = event.getScreenX();
            canvas.yEnd = event.getScreenY();
            canvas.resize(canvas.getWidth(),canvas.getHeight());
            xRotation.setValue((int)(canvas.xAngle * 180 / Math.PI) % 360);
            yRotation.setValue((int)(canvas.yAngle * 180 / Math.PI) % 360);
            canvas.xStart = canvas.xEnd;
            canvas.yStart = canvas.yEnd;
        });

    }


}


