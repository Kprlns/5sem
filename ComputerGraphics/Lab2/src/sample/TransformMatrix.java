//Капралов Н. 8о-308Б
//Матрица для преобразований

package sample;

public class TransformMatrix {
    public double matrix[][];

    public TransformMatrix(double k) {
        this.matrix = new double[4][4];
        matrix[0][0] = k;
        matrix[1][1] = k;
        matrix[2][2] = k;
        matrix[3][3] = 1;
    }

    public TransformMatrix(double phi, double psi, double theta) {
        this.matrix = new double[4][4];

        matrix[0][0] = 0;
        matrix[1][1] = 0;
        matrix[2][2] = 0;
        matrix[3][3] = 0;

        TransformMatrix[] matrices = new TransformMatrix[3];
        for (int i = 0; i < 3; i++) {
            matrices[i] = new TransformMatrix(1);
        }
        if (phi != 0.) {
            matrices[0] = this.xRotationMatrix(phi);
        }

        if (psi != 0.) {
            matrices[1] = this.yRotationMatrix(psi);

        }
        if (theta != 0.) {
            matrices[2] = this.zRotationMatrix(theta);
        }

        for (int z = 1; z < 2; z++) {
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    this.matrix[i][j] = 0;
                    for (int k = 0; k < 4; k++) {
                        this.matrix[i][j] += matrices[0].matrix[i][k] * matrices[1].matrix[k][j];
                    }

                }
            }
        }

    }

    public TransformMatrix(double[][] matrix) {
        this.matrix = new double[4][4];
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }


    public Point TransformPoint(Point point) {
        double p[] = new double[4];
        for (int i = 0; i < 4; i++) {
            p[i] =  point.getX() * matrix[0][i] + point.getY() * matrix[1][i] +
                    point.getZ() * matrix[2][i] + point.getW() * matrix[3][i];
        }
        return new Point(p[0],p[1],p[2],p[3]);
    }

    public TransformMatrix xRotationMatrix(double angle) {
        TransformMatrix matrix = new TransformMatrix(1);

        matrix.matrix[1][1] = Math.cos(angle);
        matrix.matrix[1][2] = Math.sin(angle);
        matrix.matrix[2][1] = -1 * Math.sin(angle);
        matrix.matrix[2][2] = Math.cos(angle);
        return matrix;
    }

    public TransformMatrix yRotationMatrix(double angle) {
        TransformMatrix matrix = new TransformMatrix(1);

        matrix.matrix[0][0] = Math.cos(angle);
        matrix.matrix[0][2] = -1 * Math.sin(angle);
        matrix.matrix[2][0] = Math.sin(angle);
        matrix.matrix[2][2] = Math.cos(angle);
        return matrix;
    }

    public TransformMatrix zRotationMatrix(double angle) {
        TransformMatrix matrix = new TransformMatrix(1);

        matrix.matrix[0][0] = Math.cos(angle);
        matrix.matrix[0][1] = Math.sin(angle);
        matrix.matrix[1][0] = -1 * Math.sin(angle);
        matrix.matrix[1][1] = Math.cos(angle);
        return matrix;
    }

    public void PrintMatrix() {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }
}