//Created by Kapralov Nikita
//Класс, выполняющий роль точки и вектора

package sample;

public class Point {
    private double x;
    private double y;
    private double z;
    private double w;
    public double[] coords;

    public Point(double x, double y, double z, double w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        coords = new double[] {x,y,z,w};
    }

    public Point(double[] arr) {
        x = arr[0];
        y = arr[1];
        z = arr[2];
        w = arr[3];
        coords = new double[] {x,y,z,w};
    }

    public void negate() {
        x *= -1;
        y *= -1;
        z *= -1;
        coords = new double[] {x,y,z,w};
    }



    public void print() {
        System.out.println(x + " " + y + " " + z + " " + w);
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

}
