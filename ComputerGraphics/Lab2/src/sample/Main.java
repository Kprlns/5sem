//Капралов Н. 8о-308Б
//Лабораторная работа 2
//главный класс

package sample;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample1.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setTitle("Капралов Н.С. Изометрическая проекция прямой призмы с удалением невидимых линий");
        primaryStage.setScene(scene);

        primaryStage.show();
        primaryStage.setMinWidth(scene.getWindow().getWidth());
        primaryStage.setMinHeight(scene.getWindow().getHeight());

    }


    public static void main(String[] args) {
        launch(args);
    }
}
