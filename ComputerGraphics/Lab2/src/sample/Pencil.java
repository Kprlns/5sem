//Капралов Н. 8о-308Б
//Класс, выполняющий рисовку на холсте

package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class Pencil {
    private final ResizableCanvas canvas;

    public Point zero;
    public int scale;

    public Point[] axis;

    public Point[] bottom;
    public Point[] top;

    TransformMatrix matrix;

    Pencil(ResizableCanvas canvas) {
        this.canvas = canvas;

        zero = new Point((int)canvas.getWidth() / 2,(int)canvas.getHeight() / 2,0,1);
        scale = (int)(Math.min(canvas.getWidth(),canvas.getHeight()) / 8);
        matrix = new TransformMatrix(scale);

        axis = new Point[3];
        axis[0] = new Point(1,0,0,1);
        axis[1] = new Point(0,1,0,1);
        axis[2] = new Point(0,0,1,1);

        bottom = new Point[] { //нижняя грань
                new Point(1,1,0,1),
                new Point(1,-1,0,1),
                new Point(-1,-1,0,1),
                new Point(-1,1,0,1)
        };

        top = new Point[] { //верхняя грань
                new Point(1,1,4,1),
                new Point(1,-1,4,1),
                new Point(-1,-1,4,1),
                new Point(-1,1,4,1)
        };
    }


    public void drawLine(int x1, int y1, int x2, int y2, Color color){
        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setStroke(color);
        gc.strokeLine(x1,y1,x2,y2);
    }
    public void drawLine(int x2, int y2, Color color) {
        drawLine((int)zero.getX(), (int)zero.getY(), x2, y2, color);
    }

    public void drawAxis(TransformMatrix matrix, Color[] colors, boolean flag) { //отрисовка осей
        zero.setX(canvas.getWidth() / 2);
        zero.setY(canvas.getHeight() / 2);

        if(flag) {
            axis = new Point[]{new Point(1,0,0,1), new Point(0,1,0,1), new Point(0,0,1,1)};
        }
        for (int i = 0; i < 3; i++) {
            axis[i] = matrix.TransformPoint(axis[i]);
            drawLine((int)(zero.getX() + scale * axis[i].getX()), (int)(zero.getY() - scale * axis[i].getY()), colors[i]);
        }
    }


    public void drawFigure(TransformMatrix matrix, Color color, Color[] colors) {
        Point[] tmpBtm = new Point[4];
        Point[] tmpTop = new Point[4];
        for (int i = 0; i < 4; i++) { //повороты всех точек
            tmpBtm[i] = matrix.TransformPoint(bottom[i]);
            tmpTop[i] = matrix.TransformPoint(top[i]);
        }

        int used[] = new int[4];
        for (int i = 0; i < 4; i++) {
            used[i] = 0;
        }
        Point visor = new Point(0,0,100,1); //вектор наблюдателя

        for (int i = 0; i < 4; i++) { //проверка на видимость, а затем отрисовка
            Point vector1 = new Point(tmpTop[i % 4].getX() - tmpBtm[i % 4].getX(),
                                      tmpTop[i % 4].getY() - tmpBtm[i % 4].getY(),
                                      tmpTop[i % 4].getZ() - tmpBtm[i % 4].getZ(),1);

            int j = (i + 1) % 4;
            Point vector2 = new Point(tmpBtm[i % 4].getX() - tmpBtm[j].getX(),
                                      tmpBtm[i % 4].getY() - tmpBtm[j].getY(),
                                      tmpBtm[i % 4].getZ() - tmpBtm[j].getZ(),1);
            Point normal = crossProduct(vector1,vector2);

            j = (i - 1) < 0 ? 3 : i - 1;
            Point check = new Point(tmpBtm[i % 4].getX() - tmpBtm[j].getX(),
                                    tmpBtm[i % 4].getY() - tmpBtm[j].getY(),
                                    tmpBtm[i % 4].getZ() - tmpBtm[j].getZ(),1);
            if(normal.getX() / check.getX() < 0) {
                //normal.negate();
            }

            if(scalarProduct(normal,visor) < 0) {
                for (int k = 0; k < 2; k++) {
                    int k1 = (k + i) % 4;
                    if(used[k1] == 0) {
                        used[k1]++;
                        drawLine((int)(zero.getX() + scale * tmpBtm[k1].getX()), (int)(zero.getY() - scale * tmpBtm[k1].getY()),
                                 (int)(zero.getX() + scale * tmpTop[k1].getX()), (int)(zero.getY() - scale * tmpTop[k1].getY()), color);
                    }
                }
            }
        }

        Point[][] edges;
        boolean flag = true;
        Point vector = new Point(tmpTop[0].getX() - tmpBtm[0].getX(),
                                 tmpTop[0].getY() - tmpBtm[0].getY(),
                                 tmpTop[0].getZ() - tmpBtm[0].getZ(),1);
        Point vector1 = new Point(0,0,4,1);
        double tmp = scalarProduct(vector,visor);

        if(tmp > 0) {
            edges = new Point[][] {tmpTop,tmpBtm};
        }
        else {
            edges = new Point[][] {tmpBtm,tmpTop};
        }
        for (int i = 0; i < 4; ++i) {
            int j = (i + 1) % 4;
            drawLine((int) (zero.getX() + scale * edges[0][i].getX()), (int) (zero.getY() - scale * edges[0][i].getY()),
                     (int) (zero.getX() + scale * edges[0][j].getX()), (int) (zero.getY() - scale * edges[0][j].getY()), color);
            if(used[i] + used[j] >= 2) {
                drawLine((int) (zero.getX() + scale * edges[1][i].getX()), (int) (zero.getY() - scale * edges[1][i].getY()),
                         (int) (zero.getX() + scale * edges[1][j].getX()), (int) (zero.getY() - scale * edges[1][j].getY()), color);
            }
        }
    }


    private double scalarProduct(Point a, Point b) { //скалярное произведение
        double x = a.getX() * b.getX();
        double y = a.getY() * b.getY();
        double z = a.getZ() * b.getZ();
        return x + y + z;
    }

    private Point crossProduct(Point a, Point b) { //векторное произведение
        double x = a.getY() * b.getZ() - a.getZ() * b.getY();
        double y = a.getZ() * b.getX() - a.getX() * b.getZ();
        double z = a.getX() * b.getY() - a.getY() * b.getX();
        return new Point(x, y, z,1);
    }

}
