//Капралов Н 8о-308Б
//Холст
package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ResizableCanvas extends Canvas {
    @Override
    public boolean isResizable() {
        return true;
    }

    public Color backgroundColor;
    public Color pencilColor;
    public Color chartColor;

    public double xAngle;
    public double yAngle;
    public double zAngle;

    public double xStart, yStart;
    public double xEnd, yEnd;

    Pencil pencil;

    public void resize(double width,double height){// во время изменения размера происходит отрисовка
        setWidth(width);
        setHeight(height);
        GraphicsContext gc = this.getGraphicsContext2D();
        gc.setFill(backgroundColor);
        gc.fillRect(0,0,this.getWidth(),this.getHeight());


        //Pencil pencil = new Pencil(this);


        yAngle += ((xEnd - xStart) * Math.PI / 180);
        xAngle += ((yEnd - yStart) * Math.PI / 180);

        while(yAngle > 2 * Math.PI) {
            yAngle -= (2 * Math.PI);
        }
        while(xAngle > 2 * Math.PI) {
            xAngle -= (2 * Math.PI);
        }

        while(yAngle < -2 * Math.PI) {
            yAngle += (2 * Math.PI);
        }
        while(xAngle < -2 * Math.PI) {
            xAngle += (2 * Math.PI);
        }

        zAngle = 0;

        TransformMatrix matrix = new TransformMatrix(xAngle,yAngle,zAngle);
        pencil.drawAxis(matrix, new Color[] {Color.BLUE, Color.GREEN, Color.RED}, true);
        pencil.drawFigure(matrix, Color.BLACK, new Color[] {Color.BLUE, Color.GREEN, Color.RED});

    }


    public ResizableCanvas(double width, double height) {
        super(width, height);
        xAngle = 0;
        yAngle = 0;
        zAngle = 0;

        xStart = width / 2;
        xEnd = width / 2;
        yStart = height / 2;
        yEnd = height / 2;
        pencil = new Pencil(this);
    }
}
